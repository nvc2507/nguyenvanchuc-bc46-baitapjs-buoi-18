var numArray = [];
var arr = numArray;


function addArray() {
    var input = document.querySelector("#addArr").value*1; 
    document.querySelector("#addArr").value = "";
    numArray.push(input);
    document.getElementById("result").innerHTML = numArray;
    
   
}



// câu 1
function tinhTong() {
    var sum = 0;
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] > 0) {
        sum += arr[i];
      }
    }
    
    document.getElementById("result1").innerHTML = ` Tổng các số dương : ${sum}`;
}


//Câu 2
function demSoDuong() {
  var count = 0;
  for(var i = 0;i < arr.length; i++){
    if (arr[i] > 0) {
      count++;
    }
  }
 document.getElementById("result2").innerHTML = ` Số Dương : ${count}`;
}

// Câu 3 :
function soNhoNhat() {
  var minNumber = arr[0];
  for(var i = 1; i < arr.length;i++){
    if(arr[i] < minNumber){
      minNumber = arr[i];
    }
      
  }
  document.getElementById("result3").innerHTML = ` Số Nhỏ Nhất : ${minNumber}`;
}

// Câu 4:
function timSoDuongNN() {
  var minPositive = "";
  for(var i = 0; i < arr.length;i++){
    if(arr[i] > 0 && (minPositive === "" || arr[i] < minPositive)){
      minPositive = arr[i];
    }
      
  }
  var soDuongNhoNhat = minPositive !== "" ? minPositive : "Không có số dương";
  document.getElementById("result4").innerHTML = ` Số Dương Nhỏ Nhất : ${soDuongNhoNhat}`;
}


//Câu 5 :

function soChanCuoi(){
  var soChanCuoiCung = null;
  for(var i = arr.length - 1; i >= 0; i--){
    if (arr[i] % 2 === 0){
      soChanCuoiCung = arr[i];
      break;
    }
      
  }
  var soChanCuoiCung = soChanCuoiCung !== null ? soChanCuoiCung : "-1"; 
  document.getElementById("result5").innerHTML = ` Số Chẵn Cuối : ${soChanCuoiCung}`;
  

}


// câu 6:
function doiViTri() {
  var doiCho1 = document.getElementById("doiCho1").value;
  var doiCho2 = document.getElementById("doiCho2").value;
  if  (
      doiCho1 >= 0 &&
      doiCho1 < arr.length &&
      doiCho2 >= 0 &&
      doiCho2 < arr.length &&
      doiCho1 !== doiCho2
      ){
      var temp = arr[doiCho1];
      arr[doiCho1] = arr[doiCho2];
      arr[doiCho2] = temp;
      document.getElementById("result6").innerHTML = "Mảng sau khi đổi chỗ: " + arr;
    }else{
      document.getElementById("result6").innerHTML = "Vị trí không hợp lệ hoặc giống nhau";
    }
}

// câu 7 :
function xapXep(){
  arr.sort(function(a, b) {
    return a - b;
  });
  document.getElementById("result7").innerHTML = "Mảng sau khi sắp xếp: " + arr;
}

//Câu 8:

function soNT(number) {
  if (number <= 1) {
    return false;
  }

  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      return false;
    }
  }

  return true;
}

function lastSNT(){
  var soNTDT = -1;

  for (var i = 0; i < arr.length; i++) {
    if (soNT(arr[i])) {
      soNTDT = arr[i];
      break;
    }
  }

  document.getElementById("result8").innerHTML = "Số nguyên tố đầu tiên: " + soNTDT;
}

//câu 9:

function addArray1() {
  var input = document.querySelector("#addArr1").value; 
  var elements = input.split(",").map(Number); 

  document.getElementById("result-sub").innerHTML = elements;
  demSoNguyen(elements);
}

function demSoNguyen(elements) {
  var count = 0;

  for (var i = 0; i < elements.length; i++) {
    if (Number.isInteger(elements[i])) {
      count++;
    }
  }
  document.getElementById("result9").innerHTML = "Số lượng số nguyên: " + count;
}

//câu 10:
function demSoAm() {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] < 0) {
      count++;
    }
  }
  return count;
}


function demSoDuong1() {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      count++;
    }
  }
  return count;
}

// Sử dụng hai hàm trên để so sánh số lượng số âm và dương trong mảng
function soSanh(arr) {
  const demSoAmArr = demSoAm(arr);
  const demSoDuongAr = demSoDuong1(arr);
  
  if (demSoAmArr > demSoDuongAr) {
    document.getElementById("result10").innerHTML = `Số Âm > Số Dương `;
  } else if (demSoAmArr < demSoDuongAr) {
    document.getElementById("result10").innerHTML = `Số Âm < Số Dương `;
  } else {
    document.getElementById("result10").innerHTML = `Số Âm = Số Dương `;
  }
}









 